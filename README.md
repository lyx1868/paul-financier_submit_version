# welcome to the world of Paul financier
After cloning this repository, you will have all the access to our wonderful serious game project "Paul Financier". It includes:

1, A PowerPoint showing all the details about this project (Why, What, How);

2, A folder named “Paul financier”, which is exactly the completed project folder, you should be able to load it directly on your unity.


## Some detail about the construction of the project:
You should first unzip the folder "Paul financier", and then open this completed project folder in unity.
Following the instrument of unity and then you can play the game in unity.

If you can't load the entire project folder, you can creat a empty game project in unity and replace the folder "Assets","Project_settings","User_settings" by those inside our project folder.

All the valuable work are in “Asset folder”. In Assets folder, you can see the work done by each one of us:

In the "Lucas" folder, you can find all the information regarding the mini-games in our game, as well as the assets used for their development.

In the "Leticia" folder, you can find the codes referring to the management of all events, scenes, and their impact on the action bars.

In the "Ricardo" folder, you can find the UI codes.

In the "Yuanxian" folder, you can find the codes for the GameManager, which we call "RealGameManager", which organizes all our features present in the game.


## How to start the game? 

You should start the game from the scene "Manager_Scene", located directly in the Assets folder of our game. 

One this scene is activated, all the UI elements and Minigame scenes will be activated by GameManager. Also, all of these elements are linked to the mini-game scenes in the "Lucas" folder.


## Bug?

Normally, after loading whole project, you should able to play the game.

If you meet any bug, please contact 
Yuanxian.liang@student-cs.fr


## More imformation:
The whole game is designed to be played on a Xiaomi Redmi 8*, you will probably find some misplaced blocks if you try to play on a computer. For that, choose the correct dimension (720 x 1520) for the screen game.
